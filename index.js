const randomIntArrayInRange = (min, max, n = 1) =>
  Array.from(
    { length: n },
    () => Math.floor(Math.random() * (max - min + 1)) + min
);

function swapElement(a, b) {
    var aNext = $('<div>').insertAfter(a);
    a.insertAfter(b);
    b.insertBefore(aNext);
    aNext.remove();
}

const setUpPage = arr => {
    arr.forEach((el, key) => {
        $(".array").append(`<div class='arrayEl' id='${key}' style='height: ${el}%;'><aside>${el}</aside></div>`)
    })
}

const bubbleSort = arr => {
    setUpPage(arr);
    setTimeout(() => {
        let i = 0;
        let j = 0;
        const interval = setInterval(() => {
            if (j >= arr.length - i - 1) {
                j = 0;
                i++;
            }
            if (i >= arr.length) {
                clearInterval(interval);
                console.log(arr);
                return;
            }
            if (arr[j] > arr[j + 1]) {
                var temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
                $(`#${j}`).css('height', `${arr[j]}%`).find('aside').text(arr[j]);
                $(`#${j + 1}`).css('height', `${arr[j + 1]}%`).find('aside').text(arr[j + 1]);
            }
            j++;
        }, 1000);
    }, 1000);
}

$(() => {
    let randomArray = randomIntArrayInRange(2, 96, 10);
    bubbleSort(randomArray);
});